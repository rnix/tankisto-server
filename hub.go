package main

import (
	"encoding/json"
	"fmt"
	"math"
	"time"
	//"log"
)

const (
	updateTickPeriod     = time.Second / 60
	updateSlowTickPeriod = time.Second * 5
	bulletMoveSpeed      = float64(0.3) //px/ms
)

type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	broadcast chan []byte

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client

	Map *Map
}

type PosUpdateMessage struct {
	Tanks   map[int64]TankPos `json:"tanks"`
	Bullets []Bullet          `json:"bullets"`
}

func newHub(m *Map) *Hub {
	return &Hub{
		broadcast:  make(chan []byte),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
		Map:        m,
	}
}

func (h *Hub) updateBullets() {
	for client, _ := range h.clients {
		destroyBullet := false
		if client.bullet != nil {
			b := client.bullet
			curMs := float64(time.Now().UnixNano()) / 1000000.0
			if b.MoveStartedAt > 0 {
				timeDif := curMs - b.MoveStartedAt
				moveStep := bulletMoveSpeed * timeDif
				destroyBullet = h.Map.moveBullet(b, moveStep)

			}
			b.MoveStartedAt = curMs
			if destroyBullet {
				client.bullet = nil
				h.removeBullet(client.id)
			}
		}
	}
}

func (h *Hub) GetPosUpdateMessage(onlyUpdated bool) (posUpdateMessage PosUpdateMessage) {
	posUpdateMessage.Tanks = make(map[int64]TankPos)
	posUpdateMessage.Bullets = make([]Bullet, 0)

	for client, _ := range h.clients {
		if client.isAlive && (!onlyUpdated || client.tank.Updated) {
			posUpdateMessage.Tanks[client.id] = client.tank
		}
	}

	for client, _ := range h.clients {
		if client.bullet != nil {
			posUpdateMessage.Bullets = append(posUpdateMessage.Bullets, *client.bullet)
		}
	}

	return
}

func (h *Hub) updateTicks() {
	ticker := time.NewTicker(updateTickPeriod)
	defer func() {
		ticker.Stop()
	}()
	for {
		select {
		case <-ticker.C:

			h.updateBullets()

			posUpdateMessage := h.GetPosUpdateMessage(true)

			encodedMessage, jsonEnErr := json.Marshal(posUpdateMessage)
			if jsonEnErr == nil && (len(posUpdateMessage.Tanks) > 0 || len(posUpdateMessage.Bullets) > 0) {
				h.broadcast <- encodedMessage
			}
		}
	}
}

func (h *Hub) run() {
	h.Map.hub = h
	fmt.Printf("Map width and height: %d x %d\n", h.Map.Width, h.Map.Height)
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				h.Map.clearTankLocation(client.id)
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}

func (h *Hub) removeTank(clientId int64) {
	message := fmt.Sprintf("{\"removeTank\": %d}", clientId)
	h.broadcast <- []byte(message)
}

func (h *Hub) removeBullet(clientId int64) {
	message := fmt.Sprintf("{\"removeBullet\": %d}", clientId)
	h.broadcast <- []byte(message)
}

func (h *Hub) updateTile(tileX, tileY, newGid int) {
	message := fmt.Sprintf("{\"updateTile\": {\"x\": %d, \"y\": %d, \"gid\": %d}}", tileX, tileY, newGid)
	h.broadcast <- []byte(message)
}

func (h *Hub) FindClientById(id int64) (client *Client) {
	for c := range h.clients {
		if c.id == id {
			return c
		}
	}
	return
}

func (h *Hub) dealDamageToTank(power, dstClientId int64) {
	dstClient := h.FindClientById(dstClientId)
	if dstClient != nil {
		origDstLvl := dstClient.tank.Lvl
		dstClient.tank.Lvl -= power
		if dstClient.tank.Lvl < 1 {
			h.destroyClientsTank(dstClient)

			gradePower := int64(math.Floor(float64(origDstLvl) / 3.0))
			if gradePower < 1 {
				gradePower = 1
			}
			h.spawnGrade(float64(dstClient.tank.X), float64(dstClient.tank.Y), gradePower)
		}

		messageLvls := fmt.Sprintf("{\"lvls\": [{\"id\": %d, \"lvl\": %d}]}", dstClient.id, dstClient.tank.Lvl)
		h.broadcast <- []byte(messageLvls)

	}
}

func (h *Hub) destroyClientsTank(client *Client) {
	client.isAlive = false
	h.Map.clearTankLocation(client.id)
	message := fmt.Sprintf("{\"destroyTank\": {\"id\": %d}}", client.id)
	h.broadcast <- []byte(message)
	h.removeTank(client.id)
}

func (h *Hub) spawnGrade(x, y float64, power int64) {
	grade := h.Map.spawnGrade(x, y, power)
	message := fmt.Sprintf("{\"grade\": {\"rx\": %d, \"ry\": %d, \"p\": %d}}", grade.RealX, grade.RealY, grade.Power)
	h.broadcast <- []byte(message)
}

func (h *Hub) updateSlowTicks() {
	ticker := time.NewTicker(updateSlowTickPeriod)
	defer func() {
		ticker.Stop()
	}()
	for {
		select {
		case <-ticker.C:

			tiles := h.Map.regenerateMap()
			for _, t := range tiles {
				h.updateTile(t.X, t.Y, t.Gid)
			}

		}
	}
}

func (h *Hub) clientGotGrade(clientId int64, grade *Grade) {
	client := h.FindClientById(clientId)
	if client != nil {
		client.tank.Lvl += grade.Power
		message := fmt.Sprintf("{\"removeGrade\": {\"rx\": %d, \"ry\": %d}}", grade.RealX, grade.RealY)
		h.broadcast <- []byte(message)
	}
}
