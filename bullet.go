package main

type Bullet struct {
	RawX          float64 `json:"-"`
	RawY          float64 `json:"-"`
	X             int     `json:"x"`
	Y             int     `json:"y"`
	Dr            int     `json:"dr"`
	Client        *Client `json:"-"`
	Id            int64   `json:"id"`
	MoveStartedAt float64 `json:"-"`
	Power         int64   `json:"-"`
}

func newBullet(c *Client) *Bullet {
	return &Bullet{
		RawX:   float64(c.tank.X),
		RawY:   float64(c.tank.Y),
		X:      c.tank.X,
		Y:      c.tank.Y,
		Dr:     c.tank.Dr,
		Client: c,
		Id:     c.id,
		Power:  c.tank.Lvl,
	}
}
