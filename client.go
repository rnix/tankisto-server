package main

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"math"
	"net/http"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 8096

	DirLeft  = 1
	DirRight = 2
	DirUp    = 3
	DirDown  = 4

	TankMoveSpeed = float64(0.1) //px/ms
)

var (
	newline       = []byte{'\n'}
	space         = []byte{' '}
	lastId  int64 = 0
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type IncomingMessage struct {
	Cmd      string `json:"cmd"`
	Dr       int    `json:"dr"`
	Nickname string `json:"nickname"`
}

type TankPos struct {
	X             int     `json:"x"`
	Y             int     `json:"y"`
	RawX          float64 `json:"-"`
	RawY          float64 `json:"-"`
	Dr            int     `json:"dr"`
	LastDr        int     `json:"-"`
	MoveStartedAt float64 `json:"-"`
	Lvl           int64   `json:"lvl"`
	Updated       bool    `json:"-"`
	Nickname      string  `json:"n"`
}

type OnJoinMessage struct {
	Id           int64            `json:"joinId"`
	X            int              `json:"x"`
	Y            int              `json:"y"`
	Dr           int              `json:"dr"`
	Lvl          int64            `json:"lvl"`
	MapData      Map              `json:"map"`
	PosUpdateMsg PosUpdateMessage `json:"positions"`
}

type Client struct {
	hub *Hub

	conn *websocket.Conn

	send chan []byte

	id int64

	tank TankPos

	bullet *Bullet

	isAlive bool
}

func (c *Client) move() {
	t := &c.tank

	isHorChange := (t.LastDr == DirRight || t.LastDr == DirLeft) && (t.Dr == DirDown || t.Dr == DirUp)
	isVertChange := (t.LastDr == DirUp || t.LastDr == DirDown) && (t.Dr == DirRight || t.Dr == DirLeft)
	if isHorChange || isVertChange {
		//round to 16
		t.RawX = math.Floor(t.RawX/16.0+.5) * 16.0
		t.RawY = math.Floor(t.RawY/16.0+.5) * 16.0
		t.X = int(t.RawX)
		t.Y = int(t.RawY)
		t.MoveStartedAt = 0.0
		t.Updated = true
	}

	curMs := float64(time.Now().UnixNano()) / 1000000.0
	if t.MoveStartedAt > 0 {
		timeDif := curMs - t.MoveStartedAt
		moveStep := TankMoveSpeed * timeDif

		t.RawX, t.RawY = c.hub.Map.moveWithCollision(t.RawX, t.RawY, t.Dr, moveStep, c.id)

		t.X = int(t.RawX)
		t.Y = int(t.RawY)
		t.Updated = true
	}
	t.MoveStartedAt = curMs

}

func (t *TankPos) stop() {
	t.MoveStartedAt = 0.0
	t.Updated = false
}

func (c *Client) fire() {
	if c.bullet == nil {
		c.bullet = newBullet(c)
	}
}

// readPump pumps messages from the websocket connection to the hub.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		c.hub.removeTank(c.id)
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	var im IncomingMessage

	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				//log.Printf("error: %v", err)
			}
			break
		}

		jsonErr := json.Unmarshal(message, &im)
		if jsonErr == nil && c.isAlive {
			if im.Cmd == "mv" {
				//c.tank.X = im.X
				//c.tank.Y = im.Y
				c.tank.LastDr = c.tank.Dr
				c.tank.Dr = im.Dr
				c.move()
			} else if im.Cmd == "st" {
				c.tank.stop()
			} else if im.Cmd == "f" {
				c.fire()
			} else if im.Cmd == "join" {
				nickBytes := bytes.TrimSpace(bytes.Replace([]byte(im.Nickname), newline, space, -1))
				nickString := string(nickBytes[:])
				trLength := 12
				if len(nickString) < 12 {
					trLength = len(nickString)
				}
				c.tank.Nickname = nickString[0:trLength]
				c.tank.Updated = true
			} else {
				log.Printf("unknown command %s", im.Cmd)
			}
		}
	}
}

// writePump pumps messages from the hub to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The hub closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			// Add queued chat messages to the current websocket message.
			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func (c *Client) onJoin() {
	ojm := OnJoinMessage{
		Id:           c.id,
		X:            c.tank.X,
		Y:            c.tank.Y,
		Dr:           c.tank.Dr,
		Lvl:          1,
		MapData:      *c.hub.Map,
		PosUpdateMsg: c.hub.GetPosUpdateMessage(false),
	}

	message, jsonEnErr := json.Marshal(ojm)
	if jsonEnErr == nil {
		c.send <- message
	}
}

func SpawnTank(m *Map) TankPos {
	spawnLoc := m.GetRandomEmptySpawn()
	x, y, dr := spawnLoc.GetWorldLoc(m)
	log.Printf("Spawned tank at %d, %d with dr = %d\n", x, y, dr)
	tankPos := TankPos{
		X:       x,
		Y:       y,
		RawX:    float64(x),
		RawY:    float64(y),
		Dr:      dr,
		Lvl:     1,
		Updated: true,
	}
	return tankPos
}

// serveWs handles websocket requests from the peer.
func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	lastId++
	tankPos := SpawnTank(hub.Map)
	client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256), id: lastId, tank: tankPos, isAlive: true}
	client.hub.register <- client
	go client.writePump()
	hub.Map.updateTankLocation(client.id, tankPos.RawX, tankPos.RawY)
	client.onJoin()
	client.readPump()
}
