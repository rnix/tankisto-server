package main

import (
	"flag"
	"log"
	"net/http"
)

const (
	mapFile = "map/tilemap.json"
)

var addr = flag.String("addr", ":5555", "http service address")

func main() {
	flag.Parse()
	m, err := loadMap(mapFile)
	if err != nil {
		log.Fatal("Can not load map: ", err)
	}
	hub := newHub(m)
	go hub.run()
	go hub.updateTicks()
	go hub.updateSlowTicks()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	err = http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
