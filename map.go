package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"time"
)

const (
	collisionLayerName         = "collision"
	mainLayerName              = "main"
	spawnLayerName             = "spawns"
	tankCollideRadius          = float64(16)
	GidBrick                   = 1
	GidBrickBroken             = 2
	GidCement                  = 4
	GidEmpty                   = 5
	TimeTileRegenerateCooldown = 45
)

var GidsRegenerate = map[int]bool{GidBrick: true, GidBrickBroken: true}
var GidsCollision = map[int]bool{GidBrick: true, GidBrickBroken: true, GidCement: true}

type MapLayer struct {
	Data   []int  `json:"data"`
	Name   string `json:"name"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
	Type   string `json:"type"`
	X      int    `json:"x"`
	Y      int    `json:"y"`
}

// only for clients` map sync
type MapTileset struct {
	Columns     int    `json:"columns"`
	Firstgid    int    `json:"firstgid"`
	Image       string `json:"image"`
	Imageheight int    `json:"imageheight"`
	Imagewidth  int    `json:"imagewidth"`
	Margin      int    `json:"margin"`
	Name        string `json:"name"`
	Spacing     int    `json:"spacing"`
	Tilecount   int    `json:"tilecount"`
	Tileheight  int    `json:"tileheight"`
	Tilewidth   int    `json:"tilewidth"`
}

type CollisionLoc struct {
	X int
	Y int
}

type SpawnLoc struct {
	X int
	Y int
}

type TankLoc struct {
	X        int
	Y        int
	ClientId int64
}

type Grade struct {
	X     int   `json:"-"`
	Y     int   `json:"-"`
	RealX int   `json:"rx"`
	RealY int   `json:"ry"`
	Power int64 `json:"p"`
}

type Map struct {
	Width       int          `json:"width"`
	Height      int          `json:"height"`
	Layers      []MapLayer   `json:"layers"`
	TileWidth   int          `json:"tilewidth"`
	TileHeight  int          `json:"tileheight"`
	Tilesets    []MapTileset `json:"tilesets"`
	Orientation string       `json:"orientation"`

	Collisions     []CollisionLoc `json:"-"`
	Spawns         []SpawnLoc     `json:"-"`
	TanksLocations []TankLoc      `json:"-"`
	Grades         []Grade        `json:"grades"`

	hub *Hub `json:"-"`

	InitialLayers  []MapLayer
	destroyedTiles map[Tile]int32 `json:"-"`
}

type Tile struct {
	X   int
	Y   int
	Gid int
}

type DestroyedTile struct {
	X           int
	Y           int
	Gid         int
	DestroyedAt float64
}

func loadMap(filename string) (*Map, error) {
	var m Map
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(content, &m)
	if err != nil {
		return nil, err
	}

	m.fillCollisions()
	m.fillSpawns()

	m.InitialLayers = m.Layers

	m.destroyedTiles = make(map[Tile]int32)
	m.Grades = make([]Grade, 0)

	mainLayer := m.getMainLayer()
	if mainLayer == nil {
		err = errors.New("Main layer not found")
	}

	spanwLayer := m.getSpawnLayer()
	if spanwLayer == nil {
		err = errors.New("Spawn Layer not found")
	}

	return &m, err
}

func (m *Map) addCollision(x, y int) {
	cl := CollisionLoc{X: x, Y: y}
	m.Collisions = append(m.Collisions, cl)
}

func (m *Map) setTile(tile Tile) {
	mainLayer := m.getMainLayer()

	index := tile.Y*m.Width + tile.X
	if index < 0 || index >= len(mainLayer.Data) {
		return
	}

	mainLayer.Data[index] = tile.Gid

	if tile.Gid == GidBrick || tile.Gid == GidBrickBroken {
		m.addCollision(tile.X, tile.Y)
	} else {
		m.removeCollision(tile.X, tile.Y)
	}
}

func (m *Map) fillCollisions() {
	layer := m.getMainLayer()

	m.Collisions = make([]CollisionLoc, 0)

	for i, gid := range layer.Data {
		if _, ok := GidsCollision[gid]; ok {
			x := i % m.Width
			y := int(math.Floor(float64(i) / float64(m.Width)))
			cl := CollisionLoc{X: x, Y: y}
			m.Collisions = append(m.Collisions, cl)
		}
	}
}

func (m *Map) fillSpawns() {
	spanwLayer := m.getSpawnLayer()
	if spanwLayer == nil {
		return
	}

	m.Spawns = make([]SpawnLoc, 0)

	for i, value := range spanwLayer.Data {
		if value > 0 {
			x := i % m.Width
			y := int(math.Floor(float64(i) / float64(m.Width)))
			sl := SpawnLoc{X: x, Y: y}
			m.Spawns = append(m.Spawns, sl)
		}
	}
}

func (m *Map) getLayerByName(layerName string) (layer *MapLayer) {
	for _, l := range m.Layers {
		if l.Name == layerName {
			layer = &l
			break
		}
	}

	return
}

func (sl *SpawnLoc) GetWorldLoc(m *Map) (x, y, dr int) {
	x, y = m.getWorldLocFromTiles(sl.X, sl.Y)
	x = x + int(tankCollideRadius)
	y = y + int(tankCollideRadius)
	dr = 1 + getRandomInt(4)
	return
}

func (m *Map) getMainLayer() *MapLayer {
	return m.getLayerByName(mainLayerName)
}

func (m *Map) getSpawnLayer() *MapLayer {
	return m.getLayerByName(spawnLayerName)
}

func (m *Map) isTileBlocked(tileX int, tileY int) bool {
	if tileX < 0 || tileX >= m.Width {
		return true
	}

	if tileY < 0 || tileY >= m.Height {
		return true
	}

	for _, colLoc := range m.Collisions {
		if colLoc.X == tileX && colLoc.Y == tileY {
			return true
		}
	}

	for _, tankLoc := range m.TanksLocations {
		if tankLoc.X == tileX && tankLoc.Y == tileY {
			return true
		}
	}

	return false
}

func (m *Map) isTileBlockedForClient(tileX int, tileY int, exceptClientId int64) bool {
	for _, colLoc := range m.Collisions {
		if colLoc.X == tileX && colLoc.Y == tileY {
			return true
		}
	}

	for _, tankLoc := range m.TanksLocations {
		if tankLoc.ClientId != exceptClientId && tankLoc.X == tileX && tankLoc.Y == tileY {
			return true
		}
	}

	return false
}

func Min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func Max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func FuzzyEqual(x, y, eps float64) bool {
	if x > y-eps && x < y+eps {
		return true
	}
	return false
}

//finds the farthest empty tiles
func (m *Map) findMaxEmptyTilesForMove(curTileX, curTileY, destTileX, destTileY, direction int) (maxEmptyTileX int, maxEmptyTileY int) {
	maxEmptyTileX = curTileX
	maxEmptyTileY = curTileY

	var i int

	if direction == DirRight {
		for i = curTileX; i <= destTileX+1; i = i + 1 {
			if m.isTileBlocked(i, curTileY) {
				break
			} else {
				maxEmptyTileX = i
			}
		}
	} else if direction == DirLeft {
		for i = curTileX; i >= destTileX-1; i = i - 1 {
			if m.isTileBlocked(i, curTileY) {
				break
			} else {
				maxEmptyTileX = i
			}
		}
	} else if direction == DirDown {
		for i = curTileY; i <= destTileY+1; i = i + 1 {
			if m.isTileBlocked(curTileX, i) {
				break
			} else {
				maxEmptyTileY = i
			}
		}
	} else if direction == DirUp {
		for i = curTileY; i >= destTileY-1; i = i - 1 {
			if m.isTileBlocked(curTileX, i) {
				break
			} else {
				maxEmptyTileY = i
			}
		}
	}

	if maxEmptyTileX < 0 {
		maxEmptyTileX = 0
	} else if maxEmptyTileX > m.Width {
		maxEmptyTileX = m.Width
	}

	if maxEmptyTileY < 0 {
		maxEmptyTileY = 0
	} else if maxEmptyTileY > m.Height {
		maxEmptyTileY = m.Height
	}

	return
}

func (m *Map) getMoveBorder(realX, realY float64, direction int) (border float64) {

	//curTile is top left tile of the tank
	curTileX := int(math.Floor((realX - 1.0) / float64(m.TileWidth)))
	curTileY := int(math.Floor((realY - 1.0) / float64(m.TileHeight)))

	var nextTileX, nextTileY int

	// We need next 2 tiles towards a direction
	// If they are not empty, we use tiles of the tank

	if direction == DirRight {
		if !m.isTileBlocked(curTileX+2, curTileY) && !m.isTileBlocked(curTileX+2, curTileY+1) {
			nextTileX = curTileX + 2
		} else {
			nextTileX = curTileX + 1
		}

		border = float64(nextTileX*m.TileWidth + m.TileWidth)
		border = math.Min(border, float64(m.Width*m.TileWidth))
	} else if direction == DirLeft {
		if !m.isTileBlocked(curTileX-1, curTileY) && !m.isTileBlocked(curTileX-1, curTileY+1) {
			nextTileX = curTileX - 1
		} else {
			nextTileX = curTileX
		}

		border = float64(nextTileX * m.TileWidth)
		border = math.Max(border, 0.0)
	} else if direction == DirDown {
		if !m.isTileBlocked(curTileX, curTileY+2) && !m.isTileBlocked(curTileX+1, curTileY+2) {
			nextTileY = curTileY + 2
		} else {
			nextTileY = curTileY + 1
		}

		border = float64(nextTileY*m.TileHeight + m.TileHeight)
		border = math.Min(border, float64(m.Height*m.TileHeight))
	} else if direction == DirUp {
		if !m.isTileBlocked(curTileX, curTileY-1) && !m.isTileBlocked(curTileX+1, curTileY-1) {
			nextTileY = curTileY - 1
		} else {
			nextTileY = curTileY
		}

		border = float64(nextTileY * m.TileHeight)
		border = math.Max(border, 0.0)
	}

	return
}

func (m *Map) clearTankLocation(clientId int64) {
	deleted := 0
	for i := range m.TanksLocations {
		j := i - deleted
		if m.TanksLocations[j].ClientId == clientId {
			m.TanksLocations = m.TanksLocations[:j+copy(m.TanksLocations[j:], m.TanksLocations[j+1:])]
			deleted++
		}
	}
}

func (m *Map) updateTankLocation(clientId int64, realX float64, realY float64) {
	m.clearTankLocation(clientId)

	curTileX := int(math.Floor((realX - 1.0) / float64(m.TileWidth)))
	curTileY := int(math.Floor((realY - 1.0) / float64(m.TileHeight)))

	tile1 := TankLoc{X: curTileX, Y: curTileY, ClientId: clientId}
	tile2 := TankLoc{X: curTileX + 1, Y: curTileY, ClientId: clientId}
	tile3 := TankLoc{X: curTileX, Y: curTileY + 1, ClientId: clientId}
	tile4 := TankLoc{X: curTileX + 1, Y: curTileY + 1, ClientId: clientId}

	m.TanksLocations = append(m.TanksLocations, tile1, tile2, tile3, tile4)
	m.checkGrades(clientId, curTileX, curTileY)
}

func (m *Map) moveWithCollision(realX float64, realY float64, direction int, moveStep float64, clientId int64) (float64, float64) {
	newX := realX
	newY := realY

	//restrict it to simplify calculations
	moveStep = math.Min(moveStep, float64(m.TileWidth)*0.9)

	if direction == DirLeft {
		newX -= moveStep
	} else if direction == DirRight {
		newX += moveStep
	} else if direction == DirUp {
		newY -= moveStep
	} else if direction == DirDown {
		newY += moveStep
	}

	border := m.getMoveBorder(realX, realY, direction)
	//fmt.Printf("border: %f\n", border)

	if direction == DirLeft {
		newX = math.Max(newX, border+tankCollideRadius)
	} else if direction == DirRight {
		newX = math.Min(newX, border-tankCollideRadius)
	} else if direction == DirUp {
		newY = math.Max(newY, border+tankCollideRadius)
	} else if direction == DirDown {
		newY = math.Min(newY, border-tankCollideRadius)
	}

	//fmt.Printf("newX: %f, newY: %f\n\n", newX, newY)
	m.updateTankLocation(clientId, newX, newY)

	return newX, newY
}

func (m *Map) getTilesFromWorldLoc(x, y float64) (tileX, tileY int) {
	tileX = int(math.Floor((x - 1.0) / float64(m.TileWidth)))
	tileY = int(math.Floor((y - 1.0) / float64(m.TileHeight)))
	return
}

func (m *Map) getWorldLocFromTiles(tileX, tileY int) (x, y int) {
	x = tileX * m.TileWidth
	y = tileY * m.TileHeight
	return
}

func (m *Map) getMoveStopTiles(curX, curY, newX, newY float64, direction int, clientId int64) (stop bool, stopTile1X, stopTile1Y, stopTile2X, stopTile2Y int) {
	stop = false
	stopTile1X = -1
	stopTile1Y = -1
	stopTile2X = -1
	stopTile2Y = -1

	curTileX, curTileY := m.getTilesFromWorldLoc(curX, curY)
	nextTileX, nextTileY := m.getTilesFromWorldLoc(newX, newY)

	if m.isTileBlockedForClient(curTileX, curTileY, clientId) {
		stop = true
		stopTile1X = curTileX
		stopTile1Y = curTileY
		return
	}

	if direction == DirRight || direction == DirLeft {
		if m.isTileBlockedForClient(nextTileX, nextTileY, clientId) || m.isTileBlockedForClient(nextTileX, nextTileY+1, clientId) {
			stop = true
			stopTile1X = nextTileX
			stopTile1Y = nextTileY
			stopTile2X = nextTileX
			stopTile2Y = nextTileY + 1
			return
		}
	}

	if direction == DirUp || direction == DirDown {
		if m.isTileBlockedForClient(nextTileX, nextTileY, clientId) || m.isTileBlockedForClient(nextTileX+1, nextTileY, clientId) {
			stop = true
			stopTile1X = nextTileX
			stopTile1Y = nextTileY
			stopTile2X = nextTileX + 1
			stopTile2Y = nextTileY
			return
		}
	}

	return

}

func (m *Map) moveBullet(b *Bullet, moveStep float64) (destroyBullet bool) {
	destroyBullet = false

	newX := b.RawX
	newY := b.RawY

	direction := b.Dr

	//limit it to simplify calculations (get only one next tile)
	moveStep = math.Min(moveStep, float64(m.TileWidth)*0.9)

	if direction == DirLeft {
		newX -= moveStep
	} else if direction == DirRight {
		newX += moveStep
	} else if direction == DirUp {
		newY -= moveStep
	} else if direction == DirDown {
		newY += moveStep
	}

	stop, stopTile1X, stopTile1Y, stopTile2X, stopTile2Y := m.getMoveStopTiles(b.RawX, b.RawY, newX, newY, b.Dr, b.Client.id)

	if stop {
		//fmt.Printf("Stop bullet client %d stop tiles: %d, %d\n", b.Client.id, stopTile1X, stopTile1Y)
	}

	b.RawX = newX
	b.RawY = newY
	b.X = int(b.RawX)
	b.Y = int(b.RawY)

	if stop {
		m.dealDamageToTile(b, stopTile1X, stopTile1Y)
		m.dealDamageToTile(b, stopTile2X, stopTile2Y)

		tankLoc1 := m.getTankLocOnTile(stopTile1X, stopTile1Y)
		tankLoc2 := m.getTankLocOnTile(stopTile2X, stopTile2Y)
		if tankLoc1 != nil {
			m.hub.dealDamageToTank(b.Power, tankLoc1.ClientId)
		} else if tankLoc2 != nil {
			m.hub.dealDamageToTank(b.Power, tankLoc2.ClientId)
		}
	}

	return stop
}

// locationSize - how many tiles has body with top left border in x, y
// radiusForTiles - radius to check for blocking tiles
// radiusForTanks - radius to check for any tanks
func (m *Map) checkEmptyTilesInRadius(x, y, locationSize, radiusForTiles, radiusForTanks int) bool {
	locationSize -= 1
	for i := x - radiusForTiles; i <= x+radiusForTiles+locationSize; i = i + 1 {
		for j := y - radiusForTiles; j <= y+radiusForTiles+locationSize; j = j + 1 {
			if i >= 0 && j >= 0 && i < m.Width && j < m.Height && m.isTileBlocked(i, j) {
				return false
			}
		}
	}

	for i := x - radiusForTanks; i <= x+radiusForTanks+locationSize; i = i + 1 {
		for j := y - radiusForTanks; j <= y+radiusForTanks+locationSize; j = j + 1 {
			for _, tankLoc := range m.TanksLocations {
				if tankLoc.X == i && tankLoc.Y == j {
					return false
				}
			}
		}
	}

	return true
}

func (m *Map) checkTanksInRadius(x, y, radiusForTanks int) bool {
	for i := x - radiusForTanks; i <= x+radiusForTanks; i = i + 1 {
		for j := y - radiusForTanks; j <= y+radiusForTanks; j = j + 1 {
			for _, tankLoc := range m.TanksLocations {
				if tankLoc.X == i && tankLoc.Y == j {
					return false
				}
			}
		}
	}

	return true
}

func getRandomInt(maxInt int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(maxInt)
}

func (m *Map) GetRandomEmptySpawn() SpawnLoc {
	spawns := m.Spawns

	//limit spawns to half if there are not enough players
	if len(m.hub.clients) < 4 {
		spawns = make([]SpawnLoc, 0)
		for _, sl := range m.Spawns {
			if sl.X < 30 && sl.Y < 30 {
				spawns = append(spawns, sl)
			}
		}
	}

	emptySpawns := make([]SpawnLoc, 0)
	for _, sl := range spawns {
		if m.checkEmptyTilesInRadius(sl.X, sl.Y, 2, 0, 3) {
			emptySpawns = append(emptySpawns, sl)
		}
	}

	if len(emptySpawns) > 1 {
		return emptySpawns[getRandomInt(len(emptySpawns))]
	}

	for _, sl := range spawns {
		if m.checkEmptyTilesInRadius(sl.X, sl.Y, 2, 0, 2) {
			emptySpawns = append(emptySpawns, sl)
		}
	}

	if len(emptySpawns) > 1 {
		return emptySpawns[getRandomInt(len(emptySpawns))]
	}

	for _, sl := range spawns {
		if m.checkEmptyTilesInRadius(sl.X, sl.Y, 2, 0, 1) {
			emptySpawns = append(emptySpawns, sl)
		}
	}

	if len(emptySpawns) > 1 {
		return emptySpawns[getRandomInt(len(emptySpawns))]
	}

	for _, sl := range spawns {
		if m.checkEmptyTilesInRadius(sl.X, sl.Y, 2, 0, 0) {
			emptySpawns = append(emptySpawns, sl)
		}
	}

	if len(emptySpawns) > 1 {
		return emptySpawns[getRandomInt(len(emptySpawns))]
	}

	if len(emptySpawns) == 1 {
		return emptySpawns[0]
	}

	log.Print("Not found empty spawn\n")

	return m.Spawns[0]
}

func (m *Map) getTileGid(tileX, tileY int) (gid int) {
	gid = -1
	mainLayer := m.getMainLayer()

	index := tileY*m.Width + tileX
	if index < 0 || index >= len(mainLayer.Data) {
		return
	}

	gid = mainLayer.Data[index]
	return
}

func (m *Map) removeCollision(tileX, tileY int) {
	deleted := 0
	for i := range m.Collisions {
		j := i - deleted
		if m.Collisions[j].X == tileX && m.Collisions[j].Y == tileY {
			m.Collisions = m.Collisions[:j+copy(m.Collisions[j:], m.Collisions[j+1:])]
			deleted++
		}
	}
}

func (m *Map) removeGrade(tileX, tileY int) {
	deleted := 0
	for i := range m.Grades {
		j := i - deleted
		if m.Grades[j].X == tileX && m.Grades[j].Y == tileY {
			m.Grades = m.Grades[:j+copy(m.Grades[j:], m.Grades[j+1:])]
			deleted++
		}
	}
}

func (m *Map) getTankLocOnTile(tileX, tileY int) (tc *TankLoc) {
	for _, tankLoc := range m.TanksLocations {
		if tankLoc.X == tileX && tankLoc.Y == tileY {
			tc = &tankLoc
			return
		}
	}
	return
}

func (m *Map) dealDamageToTile(b *Bullet, tileX, tileY int) {
	gid := m.getTileGid(tileX, tileY)
	newTile := Tile{X: tileX, Y: tileY, Gid: gid}
	now := int32(time.Now().Unix())

	//log.Printf("Current tile for damage %d\n", gid)
	if gid == GidBrick {
		m.destroyedTiles[newTile] = now
		newTile.Gid = GidBrickBroken
		m.setTile(newTile)
		m.hub.updateTile(tileX, tileY, GidBrickBroken)
	}
	if gid == GidBrickBroken {
		m.destroyedTiles[newTile] = now
		newTile.Gid = GidEmpty
		m.setTile(newTile)
		m.hub.updateTile(tileX, tileY, GidEmpty)
	}
}

func (m *Map) regenerateMap() (tiles []Tile) {
	now := int32(time.Now().Unix())
	for tile, destoyedAt := range m.destroyedTiles {
		if now > destoyedAt+TimeTileRegenerateCooldown {
			if _, ok := GidsRegenerate[tile.Gid]; ok && m.checkTanksInRadius(tile.X, tile.Y, 6) {
				t := Tile{X: tile.X, Y: tile.Y, Gid: GidBrick}
				m.setTile(t)
				tiles = append(tiles, t)
				delete(m.destroyedTiles, tile)
			}
		}
	}

	return
}

func (m *Map) spawnGrade(realX, realY float64, power int64) Grade {
	tileX, tileY := m.getTilesFromWorldLoc(realX, realY)
	accurateX, accurateY := m.getWorldLocFromTiles(tileX, tileY)
	g := Grade{X: tileX, Y: tileY, RealX: accurateX, RealY: accurateY, Power: power}
	m.Grades = append(m.Grades, g)
	return g
}

func (m *Map) checkGrades(clientId int64, tileX, tileY int) {
	var foundGrade *Grade
	gradeSize := 1
	for _, g := range m.Grades {
		for i := g.X; i <= g.X+gradeSize; i = i + 1 {
			for j := g.Y; j <= g.Y+gradeSize; j = j + 1 {
				if i == tileX && j == tileY {
					foundGrade = &g
					break
				}
			}
		}
	}

	if foundGrade != nil {
		m.hub.clientGotGrade(clientId, foundGrade)
		m.removeGrade(foundGrade.X, foundGrade.Y)
	}
}
